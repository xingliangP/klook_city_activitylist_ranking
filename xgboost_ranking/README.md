# 模型描述

模型采用xgboost进行训练，支持配置训练时间窗口。

本模型仅适用于城市活动页排序模型。

## 模型训练
进入xgboost_ranking目录，执行如下指令进行训练数据获取与模型训练。

其中cur_date为训练数据最后一天，days_num为数据时间窗口。
```
python train.py --cur_date '2021-01-20' --days_num 25
```
注：训练集时间加载时间较长，因此每次训练数据加载完后，会保存一份到s3上以便于后期模型性能测试。

## 模型测试
进入xgboost_ranking目录，执行如下指令得到指定模型在指定时间的预测结果。

测试用户取"SG", "HK", "TW", "MY", "TH", "VN"中各地域当日点击最多的5个用户。

每个用户召回的结果集为其当天点击量最多城市的已发布活动。
```
python get_predict_res.py \
        --predict_date_str '2021-01-21'\
        --act_base_bq_path   's3://sagemaker-studio-227729285155-ml/hubery-peng/bigquery_data/actvity_base/act_base_bp_2021_01_21.csv'\
        --model_path './models/2020-12-26_2021-01-20' \
        --res_save_prefix  './res/predict_res'
```
注：由于目前s3中没有活动名及其在各语言下的发布状态，因此需要从bigquery上获取并上到s3相应路径，这部分后期再改成自动化。 
      
bigquery 取活动当日已发布活动的活动名等字段的sql 如下：
```
select date(create_time ) as date ,activity_id , language, ifnull(activity_name_ch,activity_name_en) as activity_name,ifnull(country_name_ch,country_name_en) as country_name,ifnull(city_name_ch,city_name_en) as city_name
from `search-recommendations.data_mart.activity_base`, unnest(split( published_language ,',')) as `language` where date(create_time )=date('2021-01-16') and activity_status = "Published" and data_status = "Normal"
```
## 其它

1.非在线类型特征（如用户画像，商品画像），通过与线上推数据代码(push_feature_to_s3.py)逻辑一致来保证线上线下数据一致。

2.在线获取型特征（目前只有类别型特征，如platform,language），通过与线上数据采用相同的映射关系保证线上线下数据一致。