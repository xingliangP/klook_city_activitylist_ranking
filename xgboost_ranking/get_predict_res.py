from iht_features import load_iht_data
import pandas as pd
from iht_features import feature_warehouse
import awswrangler as wr
import datetime
import logging
import numpy as np
from iht_features.labelEncoder import load_dictionary, encoder
from xgboost import Booster, DMatrix
import pickle
import os
import argparse


def load_model(model_path):
    """加载模型训练好的模型
    :param model_path: 模型路径
    """
    model_file = os.path.join(model_path, "xgboost.model")
    logging.info("load model from %s." % model_file)
    model = Booster(model_file=model_file)
    # 加载feature_names，按训练顺序整理特征
    feature_names_file = os.path.join(model_path, "feature_names.pkl")
    logging.info("load feature names from %s" % feature_names_file)
    with open(feature_names_file, "rb") as f:
        feature_names = pickle.load(f)
    return model, feature_names

def print_percent(num):
    """将浮点型数据转化为百分数
    :param num: 数值
    """
    return str(round(num*100, 2))+"%"


def get_predict_res(date_str_in, act_base_bq_path, model_path, res_save_prefix):
    """获取"SG", "HK", "TW", "MY", "TH", "VN"六个地域的中当日点击最多的5个用户的预测结果
    :param date_str_in: 预测集时间
    :param act_base_bq_path:从bigquery倒入的文件路径
    :param model_path: 模型预测时间
    :param res_save_prefix:预测结果目录前缀
    """
    dateRaw = datetime.datetime.strptime(date_str_in, "%Y-%m-%d")
    iht_data = load_iht_data.load_sample_data(date_str_in).query('label==True')
    # 为用户点击过的活动加上destination<'country_name', 'city_name'>信息
    act_base_columns = ['activity_id', 'activity_country_id', 'activity_city_id', 'has_video', 'language',
                        'taxon_category_id', 'taxon_sub_category_id', 'taxon_leaf_category_id',
                        'taxon_category_name_en', 'taxon_sub_category_name_en', 'taxon_leaf_category_name_en']
    act_base = feature_warehouse.activity_base_features(date_str_in, columns=act_base_columns)
    # 现有的s3数据没有活动名和活动状态，需要手动上传到s3对应路径
    act_base_bq = wr.s3.read_csv(path=act_base_bq_path)
    act_base_bq['activity_id'] = act_base_bq['activity_id'].astype(str)
    act_base = pd.merge(act_base, act_base_bq, on=['activity_id'])
    iht_data = pd.merge(iht_data, act_base, on=['activity_id', 'language'], how='left')
    # 取"SG", "HK", "TW", "MY", "TH", "VN"地区点击最多的5个用户
    user_group = ['device_id', 'platform', 'language', 'ip_country_code', 'ip_city_name']
    device_clicks = iht_data[user_group+['activity_id', 'country_name', 'city_name']]\
                               .drop_duplicates()\
                               .groupby(user_group+['country_name', 'city_name']).size().reset_index(name="clicks")
    device_clicks['click_rank'] = device_clicks.groupby('ip_country_code')['clicks'].rank(ascending=False, method='first')
    check_sample = device_clicks.query('ip_country_code in ["SG", "HK", "TW", "MY", "TH", "VN"] and click_rank<=5').reset_index(drop=True)\
                                .sort_values(by=['ip_country_code', 'click_rank'])
    check_sample['click_rank'] = check_sample['click_rank'].astype(int).astype(str)
    check_sample['device_click_rank'] = check_sample['ip_country_code']+"_"+check_sample['click_rank']
    device_clicks = check_sample[['device_click_rank', 'clicks']]\
                    .rename(columns={'device_click_rank': 'country_top_click_device', 'clicks': 'click_activities_in_city'})
    # 找到测试样本用户点击最多城市下的所有活动
    check_sample = pd.merge(check_sample, act_base, on=['country_name', 'city_name', 'language'], how='left')
    # 找到测试用户的的其他特征
    profile_columns = ['device_id', 'last_day_favorite_leaf_category', 'last_month_favorite_leaf_category']
    user_profile = feature_warehouse.device_features(date_str_in, columns=profile_columns)
    # 加载活动统计特征
    # 活动的action需要做滑窗，输入的时间需要做t-1
    date_action_in = dateRaw + datetime.timedelta(days=-1)
    date_action_str = date_action_in.strftime("%Y-%m-%d")
    data_query = '(spm_page in ("City", "CityActivityListing") and spm_module == "Activity_LIST")\
                    or\
                   (click_spm_page in ("City", "CityActivityListing") and click_spm_module=="Category_LIST" and \
                   spm_page in ("SearchResult", "Experience_Vertical", "Experience_SubVertical") and \
                   spm_module in ("Activity_LIST", "SearchResults_LIST", "ThemeActivity_LIST"))'
    act_action = feature_warehouse.activity_action_features(date_action_str, 7, feature_name_suffix='weekly_recomCity',
                                                            data_query=data_query)

    check_sample = pd.merge(check_sample, act_action, on=['activity_id'], how='left')
    check_sample = pd.merge(check_sample, user_profile, on='device_id', how='left')
    logging.info("loading day data shape is (%d, %d)" % (check_sample.shape[0], check_sample.shape[1]))

    check_sample['last_day_favorite_leaf_category_top_1'] = check_sample['last_day_favorite_leaf_category'].replace(np.nan, '-999').map(
    lambda x: str(x).split(',')[0]).astype(int)
    check_sample['last_month_favorite_leaf_category_top_1'] = check_sample['last_month_favorite_leaf_category'].replace(np.nan, '-999').map(
    lambda x: str(x).split(',')[0]).astype(int)

    # 缺失值填充
    logging.info("sample data shape is (%d,%d)" % (check_sample.shape[0], check_sample.shape[1]))
    default_values = {"platform": '',
                      "language": '',
                      "ip_country_code": '',
                      "ip_city_name": '',
                      'activity_country_id': 0,
                      'activity_city_id': 0,
                      "taxon_category_id": -999,
                      "taxon_sub_category_id": -999,
                      "taxon_leaf_category_id": -999
                      }
    check_sample.fillna(value=default_values, inplace=True)
    fillna_col = ['exposure_num_weekly_recomCity', 'click_num_weekly_recomCity', 'shopping_num_weekly_recomCity',
                  'booking_num_weekly_recomCity', 'average_review_score_weekly_recomCity']
    for col in fillna_col:
        check_sample[col].replace(np.nan, 0.0, inplace=True)
    check_sample['has_video'].replace(np.nan, False, inplace=True)
    check_sample = check_sample.reset_index(drop=True)
    test_row_data = check_sample.copy()
    # 类别特征编码
    labelEncoder_dict = load_dictionary()
    check_sample = encoder(check_sample, labelEncoder_dict)
    model, feature_names = load_model(model_path)

    # 结果预测
    columns = [column for column in feature_names if
               column not in ["average_review_score_weekly_recomCity"]]
    for col in columns:
        check_sample[col] = check_sample[col].astype(int)
    check_sample['average_review_score_weekly_recomCity'] = check_sample['average_review_score_weekly_recomCity'].astype(float)

    check_data = DMatrix(check_sample[feature_names], missing=0)
    test_rank_scores = model.predict(check_data)
    test_row_data['ranking_score'] = pd.Series(test_rank_scores)
    test_row_data['score_rank'] = test_row_data.groupby('device_id')['ranking_score'].rank(ascending=False,method='first')

    res = pd.merge(test_row_data.drop(columns=['date']).query('score_rank<=100').drop_duplicates(),\
             iht_data[['device_id', 'platform', 'language', 'ip_country_code',
                        'ip_city_name', 'country_name', 'city_name', 'activity_id', 'label']].drop_duplicates(),\
             on = ['device_id', 'platform', 'language', 'ip_country_code',
                        'ip_city_name', 'country_name', 'city_name', 'activity_id'], how='left')\
            .sort_values(by=["device_click_rank", "ranking_score"], ascending=[True,False])\
            .reset_index(drop=True)\
            .rename(columns={'label': 'is_catch', 'city_name': 'recall_city_name'})
    res['is_catch'] = res['is_catch'].fillna(False)
    # 获取各个地域的预测百分比
    sg_top_10 = print_percent(res.query('score_rank<=10 and ip_country_code =="SG"').query('is_catch==True').shape[0]/50)
    hk_top_10 = print_percent(res.query('score_rank<=10 and ip_country_code =="HK"').query('is_catch==True').shape[0]/50)
    tw_top_10 = print_percent(res.query('score_rank<=10 and ip_country_code =="TW"').query('is_catch==True').shape[0]/50)
    my_top_10 = print_percent(res.query('score_rank<=10 and ip_country_code =="MY"').query('is_catch==True').shape[0]/50)
    th_top_10 = print_percent(res.query('score_rank<=10 and ip_country_code =="TH"').query('is_catch==True').shape[0]/50)
    vn_top_10 = print_percent(res.query('score_rank<=10 and ip_country_code =="VN"').query('is_catch==True').shape[0]/50)
    all_top_10 = print_percent(res.query('score_rank<=10').query('is_catch==True').shape[0]/50/6)
    sg_top_20 = print_percent(res.query('score_rank<=20 and ip_country_code =="SG"').query('is_catch==True').shape[0]/100)
    hk_top_20 = print_percent(res.query('score_rank<=20 and ip_country_code =="HK"').query('is_catch==True').shape[0]/100)
    tw_top_20 = print_percent(res.query('score_rank<=20 and ip_country_code =="TW"').query('is_catch==True').shape[0]/100)
    my_top_20 = print_percent(res.query('score_rank<=20 and ip_country_code =="MY"').query('is_catch==True').shape[0]/100)
    th_top_20 = print_percent(res.query('score_rank<=20 and ip_country_code =="TH"').query('is_catch==True').shape[0]/100)
    vn_top_20 = print_percent(res.query('score_rank<=20 and ip_country_code =="VN"').query('is_catch==True').shape[0]/100)
    all_top_20 = print_percent(res.query('score_rank<=20').query('is_catch==True').shape[0]/100/6)

    data = {'ip_country_code': ["SG", "HK", "TW", "MY", "TH", "VN", "all_top"],
            'country_precision@10': [sg_top_10, hk_top_10, tw_top_10, my_top_10, th_top_10, vn_top_10, all_top_10],
            'country_precision@20': [sg_top_20, hk_top_20, tw_top_20, my_top_20, th_top_20, vn_top_20, all_top_20]}
    precision = pd.DataFrame(data)
    print('cur_date is %s' % date_str_in)
    print(precision)

    res = pd.merge(res, device_clicks, left_on=['device_click_rank'], right_on=['country_top_click_device'])
    act_activities = act_base.groupby(['activity_country_id', 'activity_city_id', 'language']).size().reset_index(name='recall_activities')
    res = pd.merge(res, act_activities, on=['activity_country_id', 'activity_city_id', 'language'],how='left')
    res['ctr'] = res['click_num_weekly_recomCity']/res['exposure_num_weekly_recomCity']
    res['cvr'] = res['booking_num_weekly_recomCity']/res['click_num_weekly_recomCity']
    res['device_click_ration'] = res['click_activities_in_city']/res['recall_activities']

    save_path_suffix = model_path.split('/')[-1] + '_predict_' + date_str_in + '_res_100.csv'
    if not os.path.exists(res_save_prefix):
        os.makedirs(res_save_prefix)
    out_columns_order = ['country_name', 'device_id', 'score_rank', 'is_catch', 'ip_city_name',
                         'recall_city_name', 'recall_activities', 'click_activities_in_city', 'device_click_ration', 'platform', 'language',
                         'activity_id', 'has_video', 'average_review_score_weekly_recomCity', 'shopping_num_weekly_recomCity', 'booking_num_weekly_recomCity',
                         'exposure_num_weekly_recomCity', 'click_num_weekly_recomCity', 'ctr', 'cvr',
                         'activity_name', 'taxon_category_name_en', 'taxon_sub_category_name_en', 'taxon_leaf_category_name_en', 'taxon_leaf_category_id',
                         'last_day_favorite_leaf_category_top_1', 'last_month_favorite_leaf_category_top_1']
    res[out_columns_order].to_csv(res_save_prefix+'/'+save_path_suffix, index=False)


def main(**params):
    get_predict_res(params['predict_date_str'], params['act_base_bq_path'], params['model_path'], params['res_save_prefix'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    today = datetime.datetime.now()
    today_str = today.strftime("%Y-%m-%d")
    parser.add_argument("--predict_date_str", default=today_str, help="predict day", type=str)
    parser.add_argument("--act_base_bq_path", default='s3://sagemaker-studio-227729285155-ml/hubery-peng/bigquery_data/actvity_base/act_base_bp_2021_01_16.csv', help="act_base_bq_path ", type=str)
    parser.add_argument("--model_path", help="model path ", type=str)
    parser.add_argument("--res_save_prefix", help="predict res save prefix ", type=str)
    args = parser.parse_args()
    main(**{"predict_date_str": args.predict_date_str, "act_base_bq_path": args.act_base_bq_path, "model_path": args.model_path, "res_save_prefix": args.res_save_prefix})