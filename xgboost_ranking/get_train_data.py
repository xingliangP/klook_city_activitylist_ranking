from iht_features.labelEncoder import load_dictionary, encoder
from iht_features import feature_warehouse, load_iht_data
import time
import logging
import datetime
import random
import pandas as pd
import numpy as np
import numexpr

numexpr.set_num_threads(numexpr.detect_number_of_cores())

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)


def load_train_data(cur_date_str, days_num=30):
    """加载train特征数据
    :param cur_date_str: 指定数据日期最后一天
    :param days_num: 训练窗口大小
    """
    logging.info("loading train data...")
    load_start_time = time.time()
    iht_data = pd.DataFrame()
    dateRaw = datetime.datetime.strptime(cur_date_str, "%Y-%m-%d")
    for i in list(range(days_num))[::-1]:
        dateRaw_in = dateRaw + datetime.timedelta(days=-i)
        date_str_in = dateRaw_in.strftime("%Y-%m-%d")
        logging.info("current loading train data is %s" % date_str_in)
        sub_iht_data = load_iht_data.load_sample_data(date_str_in)
        act_base_columns = ['activity_id', 'activity_country_id', 'activity_city_id', 'has_video',
                            'taxon_category_id', 'taxon_sub_category_id', 'taxon_leaf_category_id']
        act_base = feature_warehouse.activity_base_features(date_str_in, columns=act_base_columns)
        act_base['activity_id'] = act_base['activity_id'].astype(str)

        profile_columns = ['device_id', 'last_day_favorite_leaf_category', 'last_month_favorite_leaf_category']
        user_profile = feature_warehouse.device_features(date_str_in, columns=profile_columns)

        # 活动的action需要做滑窗，输入的时间需要做t-1
        date_action_in = dateRaw_in + datetime.timedelta(days=-1)
        date_action_str = date_action_in.strftime("%Y-%m-%d")
        data_query = '(spm_page in ("City","CityActivityListing") and spm_module == "Activity_LIST")\
                        or\
                       (click_spm_page in ("City","CityActivityListing") and click_spm_module=="Category_LIST" and \
                       spm_page in ("SearchResult","Experience_Vertical","Experience_SubVertical") and \
                       spm_module in ("Activity_LIST","SearchResults_LIST","ThemeActivity_LIST"))'
        act_action = feature_warehouse.activity_action_features(date_action_str, 7,
                                                                feature_name_suffix='weekly_recomCity',
                                                                query=data_query) \
            .drop(columns=['favourite_num_weekly_recomCity'])

        sub_iht_data = pd.merge(sub_iht_data, act_base, on=['activity_id'], how='left')
        sub_iht_data = pd.merge(sub_iht_data, act_action, on=['activity_id'], how='left')
        sub_iht_data = pd.merge(sub_iht_data, user_profile, on='device_id', how='left')
        logging.info("loading day data shape is (%d,%d)" % (sub_iht_data.shape[0], sub_iht_data.shape[1]))
        iht_data = iht_data.append(sub_iht_data)

    iht_data['last_day_favorite_leaf_category_top_1'] = iht_data['last_day_favorite_leaf_category'].replace('', '0') \
        .map(lambda x: str(x).split(',')[0]).astype(int)
    iht_data['last_month_favorite_leaf_category_top_1'] = iht_data['last_month_favorite_leaf_category'].replace('', '0') \
        .map(lambda x: str(x).split(',')[0]).astype(int)

    logging.info("train windows sample shape is (%d,%d)" % (iht_data.shape[0], iht_data.shape[1]))

    # 缺失值填充
    default_values = {"platform": '',
                      "language": '',
                      "ip_country_code": '',
                      "ip_city_name": '',
                      'activity_country_id': 0,
                      'activity_city_id': 0}
    iht_data.fillna(value=default_values, inplace=True)
    fillna_col = ['exposure_num_weekly_recomCity', 'click_num_weekly_recomCity', 'shopping_num_weekly_recomCity',
                  'booking_num_weekly_recomCity', 'average_review_score_weekly_recomCity']
    for col in fillna_col:
        iht_data[col].replace(np.nan, 0.0, inplace=True)
    iht_data['has_video'].replace(np.nan, False, inplace=True)
    iht_data = iht_data.reset_index(drop=True)

    # 将未切割的训练数据到s3中，
    # 保持模型路径为训练集合的起始时间
    dateRaw = datetime.datetime.strptime(cur_date_str, "%Y-%m-%d")
    start_data = dateRaw + datetime.timedelta(days=-days_num)
    start_data = start_data.strftime("%Y-%m-%d")
    trian_window = start_data + '_' + cur_date_str
    save_path_prefix = "s3://sagemaker-studio-227729285155-ml/hubery-peng/train_data/" + trian_window
    logging.info("train windows sample save to s3 path is %s" % save_path_prefix)
    iht_data.to_parquet(save_path_prefix + '/train_data.parquet', index=False)
    # 类别特征编码
    labelEncoder_dict = load_dictionary()
    iht_data = encoder(iht_data, labelEncoder_dict)
    # 字段顺序调整
    columns = iht_data.columns.values.tolist()
    columns = [column for column in columns if
               column not in ["label", "qid", 'device_id', 'last_day_favorite_leaf_category',
                              'last_month_favorite_leaf_category']]
    columns = ["label", "qid"] + columns
    iht_data = iht_data[columns]

    # 删除指定列
    delete_columns = ["activity_id"]
    iht_data = iht_data.drop(columns=delete_columns)

    # 切分数据集: train:test = 8:2
    logging.info("Divide the sample...")
    device_ids = iht_data["qid"].unique()
    random.shuffle(device_ids)
    test_rate = 0.2
    test_size = int(len(device_ids) * test_rate)
    logging.info("train:test rate is %s:%s" % ((1 - test_rate) * 10, test_rate * 10))
    test_data = iht_data[iht_data["qid"].isin(device_ids[:test_size])].reset_index(drop=True)
    train_data = iht_data[iht_data["qid"].isin(device_ids[test_size:])].reset_index(drop=True)
    logging.info("train_data shape is (%d,%d)" % (train_data.shape[0], train_data.shape[1]))
    logging.info("test_data shape is (%d,%d)" % (test_data.shape[0], test_data.shape[1]))
    # 打印训练集与验证集生成时间
    logging.info("load train data end")
    load_end_time = time.time()
    logging.info('load train data 总时时长: %.2f 分钟' % ((load_end_time - load_start_time) / 60))
    return (train_data, test_data)