import os
import pandas as pd
import time
pd.options.mode.chained_assignment = None ##临时方案


dst_prod = "s3://klook-big-data-prod/data/processed"
def load_sample_data(dateStr):
    year, month, day = dateStr.split("-")
    sh_exp_download = "aws s3 cp {dst}/in-house-tracking/event_exposure/year={year}/month={month}/day={day}/ \
                       ./dataset/event_exposure/year={year}/month={month}/day={day} --recursive  > ./dataset/download/cp.log ".format(dst=dst_prod, year=year, month=month, day=day)
    sh_delete_null_file = "find ./dataset/* -name \"*\" -type f -size 0c  | xargs -n 1 rm -f"

    dict_lang = {'de': 'de_DE', 'es': 'es_ES', 'fr': 'fr_FR', 'id': 'id_ID', 'it': 'it_IT', 'ja': 'ja_JP',
                 'ru': 'ru_RU',
                 'vi': 'vi_VN', 'th': 'th_TH', 'ko': 'ko_KR', 'en': 'en_BS'
                 }
    select_columns = ["device_id", "uid", "session_id", "platform", "language", "ip_country_code",
                      'ip_city_name', "module_object_id", "spm_page", "spm_module", "click_spm_page",
                      'click_spm_module', "module_object_id_type", "site_name"]
    data_filter_1 = [('spm_page', 'in', ("City", "CityActivityListing")), ('spm_module', '=', 'Activity_LIST'),
                     ('module_object_id_type', '=', 'activity'), ('module_object_id', '!=', '')]
    data_filter_2 = [('click_spm_page', 'in', ("City", "CityActivityListing")),
                     ('click_spm_module', '=', 'Category_LIST'),
                     ('spm_page', 'in', ("SearchResult", "Experience_Vertical", "Experience_SubVertical")),
                     ('spm_module', 'in', ("Activity_LIST", "SearchResults_LIST", "ThemeActivity_LIST")),
                     ('module_object_id_type', '=', 'activity'), ('module_object_id', '!=', '')]
    # 加载曝光数据并进行处理
    if os.system(sh_exp_download) == 0:
        if os.system(sh_delete_null_file) == 0:
            cur_path = './dataset/event_exposure/year={year}/month={month}/day={day}'.format(year=year, month=month, day=day)
            pd_exposure = pd.DataFrame()
            pd_exposure_1 = pd.read_parquet(cur_path, columns=select_columns+ ['duration'], filters=data_filter_1)
            pd_exposure_2 = pd.read_parquet(cur_path, columns=select_columns+ ['duration'], filters=data_filter_2)
            # 删除文件
            sh_delete_file = 'rm -rf ./dataset/event_exposure/'
            os.system(sh_delete_file)
            pd_exposure = pd_exposure.append(pd_exposure_1)
            pd_exposure = pd_exposure.append(pd_exposure_2)
            # device_id与session_id不能为空 & 站点控制 & 曝光时间控制
            pd_exposure = pd_exposure.query('device_id.notnull() and device_id !=""  and session_id.notnull() and session_id !=""\
                                            and site_name in ("klook","www.klook.com") and duration>=1000', engine='python')
            # 语言多端统一
            pd_exposure.loc[:,'language'] = pd_exposure['language'].map(lambda x: x.replace("-", "_")) \
                                                             .map(lambda x: dict_lang[x] if x in dict_lang else x)
            # 曝光数据写为False
            pd_exposure.loc[:,'label'] = False
            pd_exposure = pd_exposure.rename(columns={'module_object_id': 'activity_id'})
            pd_exposure = pd_exposure[['label', 'activity_id', 'device_id', 'uid', 'session_id', 'platform', 'language',
                                       'ip_country_code', 'ip_city_name']]
    # 加载点击数据并进行处理
    sh_click_download = "aws s3 cp {dst}/in-house-tracking/event_click/year={year}/month={month}/day={day}/ \
                       ./dataset/event_click/year={year}/month={month}/day={day} --recursive > ./dataset/download/cp.log ".format(dst=dst_prod, year=year, month=month, day=day)
    if os.system(sh_click_download) == 0:
        if os.system(sh_delete_null_file) == 0:
            if os.system(sh_delete_null_file) == 0:
                cur_path = './dataset/event_click/year={year}/month={month}/day={day}'.format(year=year, month=month,
                                                                                              day=day)
                # 取城市数据
                pd_action = pd.DataFrame()
                pd_action_1 = pd.read_parquet(cur_path, columns=select_columns, filters=data_filter_1)
                pd_action_2 = pd.read_parquet(cur_path, columns=select_columns, filters=data_filter_2)
                # 删除文件
                sh_delete_file = 'rm -rf ./dataset/event_click/'
                os.system(sh_delete_file)
                pd_action = pd_action.append(pd_action_1)
                pd_action = pd_action.append(pd_action_2)
                # device_id与session_id不能为空
                pd_action = pd_action.query('device_id.notnull() and device_id !=""  and session_id.notnull() and session_id !=""\
                                            and site_name in ("klook","www.klook.com")', engine='python')
                # device_id与session_id不能为空 & 站点控制
                # 语言多端统一
                pd_action.loc[:,'language'] = pd_action['language'].map(lambda x: x.replace("-", "_")) \
                                                             .map(lambda x: dict_lang[x] if x in dict_lang else x)
                # 点击数据写为True
                pd_action.loc[:,'label'] = True
                pd_action = pd_action.rename(columns={'module_object_id': 'activity_id'})
                pd_action = pd_action[['label', 'activity_id', 'device_id', 'uid', 'session_id', 'platform', 'language',
                                       'ip_country_code', 'ip_city_name']]

        # 按session获取正负样本
        iht_features = ['activity_id', 'device_id', 'uid', 'session_id', 'platform', 'language', 'ip_country_code', 'ip_city_name']
        # 取正样本
        pd_action = pd_action[iht_features].drop_duplicates().reset_index(drop=True)
        pd_action.loc[:,'label'] = True
        # 取负样本
        pd_exposure = pd.merge(pd_exposure[iht_features], pd_action, on=iht_features, how='left').query('label!=True')[iht_features] \
                        .drop_duplicates().reset_index(drop=True)
        pd_exposure.loc[:,'label'] = False
        iht_data = pd_exposure.append(pd_action)
        # 按device_id&uid&session_id分组
        iht_data['uid'].fillna('-1',inplace=True)
        iht_data['qid'] = iht_data['device_id'] + '&&&' + iht_data['session_id']
        iht_data = iht_data.drop(columns=['uid', 'session_id'])
        # activity_id必须为数值型
        iht_data = iht_data[pd.to_numeric(iht_data['activity_id'], errors='coerce').notnull()]
        iht_data['activity_id'] = iht_data['activity_id'].astype(str)
    return iht_data