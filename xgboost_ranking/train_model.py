# coding: utf-8

import os
import logging
import pickle
import numpy as np
import pandas as pd
import datetime
from matplotlib import pyplot as plt
from xgboost import DMatrix, train
from xgboost import plot_importance
from get_train_data import load_train_data
import time
import argparse


def train_model(dtrain, evals, model_file, save_model):
    params = {
        "booster": "gbtree",
        "objective": "rank:pairwise",
        "max_depth": 8,
        "silent": 0,
        "learning_rate": 0.1,
        "nthread": -1,
        "eval_metric": ["auc"]
    }

    logging.info("train shape: [%s, %s]" % (dtrain.num_row(), dtrain.num_col()))

    #     模型训练
    logging.info("train model...")
    evals_result = {}
    xgb_model = None
    gbm = train(params, dtrain, evals=evals, xgb_model=xgb_model, num_boost_round=1000, early_stopping_rounds=None,
                evals_result=evals_result)

    # 保存模型
    if save_model:
        gbm.save_model(model_file)
        logging.info("save model to %s." % model_file)

    # plot_importance(gbm)
    # plt.show()

    return evals_result, gbm, params


def generate_dataset(df):
    """生成数据集.
    """
    # df数据分布: 0,label/y; 1,qid; 2~n,features
    feature_names = df.columns.values.tolist()[2:]
    data = df.to_numpy()

    y = data[:, 0]
    qid = data[:, 1]
    X = data[:, 2:]

    # 数据分组
    _, group_sizes = np.unique(qid, return_counts=True)
    indices = qid.argsort()  # 从小到大排序

    X = X[indices]
    y = y[indices]

    outputs = DMatrix(X, label=y, feature_names=feature_names)
    outputs.set_group(group_sizes)
    return outputs


def print_result(results):
    print_str = "eval_set: %s, eval_metric: %s, max value: %s, final value: %s."
    eval_result_items = []
    for eval_set in results:
        items = []
        for eval_metric in results[eval_set]:
            values = results[eval_set][eval_metric]
            max_value = max(values)
            final_value = values[-1]
            logging.info(print_str % (eval_set, eval_metric, max_value, final_value))
            items.append({
                "eval_metric": eval_metric,
                "max_value": max_value,
                "final_value": final_value
            })
        eval_result_items.append({
            "evalSet_name": eval_set,
            "metrics_items": items
        })
    return eval_result_items


def main(**params):
    # 生成训练集
    logging.info("generate train set...")
    cur_date = params['cur_date']
    days_num = params['days_num']
    (train_data, test_data) = load_train_data(cur_date, days_num)
    logging.info("train set shape: %s" % str(train_data.shape))
    dtrain = generate_dataset(train_data)
    logging.info("test set shape: %s" % str(test_data.shape))
    dtest = generate_dataset(test_data)
    evals = [(dtest, "test")]
    # 保持模型路径为训练集合的起始时间
    dateRaw = datetime.datetime.strptime(cur_date, "%Y-%m-%d")
    start_data = dateRaw + datetime.timedelta(days=-days_num)
    start_data = start_data.strftime("%Y-%m-%d")
    model_prefile = start_data + '_' + cur_date
    logging.info("model_prefile path is : %s" % model_prefile)
    model_path = os.path.join(os.getcwd(), "models/%s" % model_prefile)
    if not os.path.exists(model_path):
        os.makedirs(model_path)
    start_time = time.time()
    save_model_file = os.path.join(model_path, "xgboost.model")
    results, model, model_params = train_model(dtrain, evals, save_model_file, True)
    eval_result = print_result(results)
    end_time = time.time()
    logging.info('time is %s ' % round((end_time - start_time) / 60, 3))
    # 保存模型特征权重
    fig, ax = plt.subplots(figsize=(12, 20))
    plot_importance(model,
                    height=0.5,
                    ax=ax,
                    max_num_features=64)
    features_importance_path = os.path.join(model_path, "features_importance.png")
    plt.savefig(features_importance_path, dpi=200)
    plt.show()

    # 将特征名和顺序保存下来
    feature_names_file = os.path.join(model_path, "feature_names.pkl")
    with open(feature_names_file, "wb") as f:
        feature_names = train_data.columns.values.tolist()[2:]
        pickle.dump(feature_names, f)
    logging.info("save feature names to %s." % feature_names_file)

    # 曝光训练结果
    df_metrics = pd.DataFrame(eval_result[0]['metrics_items'])
    df_metrics['训练集合时间区间'] = model_prefile
    mer_columns = df_metrics.columns.tolist()

    mer_columns = [column for column in mer_columns if column not in ["训练集合时间区间"]]
    mer_columns = ["训练集合时间区间"] + mer_columns
    df_metrics[mer_columns].to_csv(model_path + '/metrics.csv', index=False)
    logging.info("save df_metrics names to %s." % model_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    yesterday = datetime.datetime.now() + datetime.timedelta(days=-1)
    yesterday_str = yesterday.strftime("%Y-%m-%d")
    parser.add_argument("--cur_date", default=yesterday_str, help="train last date", type=str)
    parser.add_argument("--days_num", default=7, help="train duration", type=int)
    args = parser.parse_args()
    main(**{"cur_date": args.cur_date, "days_num": args.days_num})